package zkosskeycloaktest.vm;

import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zk.ui.Executions;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;

public class IndexViewModel {
    private Integer i = 0;

    @Init
    public void init() {

    }

    @Command
    public void logout() {
        try {
            HttpServletRequest servletRequest = (HttpServletRequest) Executions.getCurrent().getNativeRequest();

            servletRequest.logout();
            Executions.sendRedirect("");
        } catch (ServletException e) {
            e.printStackTrace();
        }
    }

    @Command
    @NotifyChange("valueOfI")
    public void doSomething() {
        this.i += 1;
    }

    public Integer getValueOfI() {
        return i;
    }
}
